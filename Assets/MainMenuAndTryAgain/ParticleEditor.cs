﻿// Gets a particle system reference from one attached in the inspector
// At 2 and 4 secs it adds a burst of 100 particles
using UnityEngine;
using System.Collections;

public class ParticleEditor : MonoBehaviour {
	void Start() {
		ParticleSystem ps = GetComponent<ParticleSystem>();
		var em = ps.emission;
		em.enabled = true;

		em.type = ParticleSystemEmissionType.Time;

		em.SetBursts(
			new ParticleSystem.Burst[]{
				new ParticleSystem.Burst(2.0f, 100),
				new ParticleSystem.Burst(4.0f, 100)
			});
	}
}