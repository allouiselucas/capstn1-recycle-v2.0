﻿using UnityEngine;
using System.Collections;

public class ParticleEmission : MonoBehaviour {


		void Start() {
			ParticleSystem ps = GetComponent<ParticleSystem>();
			var em = ps.emission;
			em.enabled = true;

			em.type = ParticleSystemEmissionType.Time;

			em.SetBursts(
				new ParticleSystem.Burst[]{
					new ParticleSystem.Burst(2.0f, 100),
					new ParticleSystem.Burst(4.0f, 100)
				});
		}


}
