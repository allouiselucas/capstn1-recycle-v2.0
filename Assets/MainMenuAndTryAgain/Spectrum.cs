﻿using UnityEngine;
using System.Collections;

public class Spectrum : MonoBehaviour {

	public GameObject prefab;
	public int numberOfObjects;
	public float radius ;
	public int strength  ;
	public GameObject[] cubes;
    public GameObject cubeHolder;
//	public Material mat;
//	public float r;
//	public float g;
//	public float b;


	void Start() {

        cubeHolder = GameObject.Find("CubeHolder").gameObject;

		//r = mat.color.r;
		//g = mat.color.g;
		//b = mat.color.b;


		//https://docs.unity3d.com/Manual/InstantiatingPrefabs.html

		for (int i = 0; i < numberOfObjects; i++) {
			float angle = i * Mathf.PI * 2 / numberOfObjects;
			Vector3 pos = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * radius;
			GameObject temp = Instantiate(prefab, pos, Quaternion.identity);
            temp.transform.parent = cubeHolder.transform;
            temp.AddComponent<RotationScript>();
            temp.GetComponent<RotationScript>().rotationSpeedy = 8  ;
		}


		cubes = GameObject.FindGameObjectsWithTag ("Cube");
	}



	void Update () {
		float[] spectrum = AudioListener.GetSpectrumData (1024, 0, FFTWindow.Hamming);
		for (int i = 0; i < numberOfObjects; i++) {
			Vector3 previousScale = cubes [i].transform.localScale;
			previousScale.y = Mathf.Lerp (previousScale.y, spectrum [i] * 200, Time.deltaTime * strength);
			cubes [i].transform.localScale = previousScale;
		}
	}



	void OnGUI()
	{
/*		
		r = GUI.HorizontalSlider (new Rect (20, 10, Screen.width - 40, 20), r, 0.0f, 1.0f);
		g = GUI.HorizontalSlider (new Rect (20, 30, Screen.width - 40, 20), g, 0.0f, 1.0f);
		b = GUI.HorizontalSlider (new Rect (20, 50, Screen.width - 40, 20), b, 0.0f, 1.0f);

		mat.color = new Color (r, g, b);
*/
	}
}