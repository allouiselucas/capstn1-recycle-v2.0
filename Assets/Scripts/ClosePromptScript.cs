﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosePromptScript : MonoBehaviour {


    private float curTime = 0.0f;
    private float timeToClose = 3.0f;

    void Start()
    {
        curTime = timeToClose;
    }
	// Update is called once per frame
	void Update ()
    {
        if (this.gameObject.activeSelf)
        {
            curTime -= Time.deltaTime;
            if (curTime <= 0.0f)
            {
                curTime = timeToClose;
                this.gameObject.SetActive(false);
            }
        }
	}
}
