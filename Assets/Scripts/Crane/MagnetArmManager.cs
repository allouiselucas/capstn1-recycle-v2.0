﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetArmManager : MonoBehaviour
{


    public MagnetGameManager gameManager;

    public int moveDirection;

    public float acce;
    public float acceLow;

    public GameObject hookedObject;

    public GameObject startPoint;
    public GameObject leftPoint;
    public GameObject centerPoint;
    public GameObject rightPoint;
    public GameObject lowestPoint;

    public GameObject magnetHand;
    public GameObject panel;

    public Vector3 tempPos;

    public bool magnetOn;
    public bool operational;
    

    public _armState armState;

    public enum _armState
    {
        NotMovingVertical,
        Pull,
        PullDown,
        PullUp,
        PullLeft,
        PullRelease,
        End


    }






    public void MoveArmHorizontal(int direction)
    {
        switch (direction)
        {
            case -1:
                {
                    this.GetComponent<RectTransform>().transform.position =
                        Vector3.MoveTowards(this.GetComponent<RectTransform>().transform.position,
                        leftPoint.GetComponent<RectTransform>().transform.position, acce * Time.deltaTime);

                    if (this.GetComponent<RectTransform>().transform.position == leftPoint.GetComponent<RectTransform>().transform.position) moveDirection = 0;
                    break;
                }
            case 1:
                {
                    this.GetComponent<RectTransform>().transform.position =
                        Vector3.MoveTowards(this.GetComponent<RectTransform>().transform.position,
                        rightPoint.GetComponent<RectTransform>().transform.position, acce * Time.deltaTime);

                    if (this.GetComponent<RectTransform>().transform.position == rightPoint.GetComponent<RectTransform>().transform.position) moveDirection = 0;
                    break;
                }

        }
    }

    public void MoveLeft()
    {
        if (armState == _armState.NotMovingVertical)
        {
            if (operational)
                moveDirection--;
        }
    }

    public void MoveRight()
    {
        if (armState == _armState.NotMovingVertical)
        {
            if (operational)
                moveDirection++;
        }
    }

    public void Pull()
    {
        if (armState == _armState.NotMovingVertical)
            armState = _armState.Pull;
    }

    public void Magnetize(GameObject hookedObj)
    {
        if (armState != _armState.End)
        {
            hookedObj.GetComponent<RectTransform>().transform.position =
                           Vector3.MoveTowards(hookedObj.GetComponent<RectTransform>().transform.position,
                           this.GetComponent<RectTransform>().transform.position, 100 * Time.deltaTime);
        }
    }


    void Update()
    {
        if (operational)
        {
            moveDirection = Mathf.Clamp(moveDirection, -1, 1);

            if (hookedObject != null)
            {
                Magnetize(hookedObject);
            }

            if (moveDirection == 0)
            {
                acce = acceLow;
            }

            if (moveDirection != 0)
            {
                acce += acceLow * 0.05f;
                acce = Mathf.Clamp(acce, acceLow, acceLow * 5);
            }

            switch (armState)
            {


                case _armState.NotMovingVertical:
                    {
                        magnetHand.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;

                        if (moveDirection != 0)
                        {
                            MoveArmHorizontal(moveDirection);
                        }
                        break;
                    }
                case _armState.Pull:
                    {
                        magnetOn = true;
                        moveDirection = 0;
                        tempPos = this.transform.position;

                        armState = _armState.PullDown;
                        break;
                    }
                case _armState.PullDown:
                    {



                        magnetHand.GetComponent<RectTransform>().transform.position =
                      Vector3.MoveTowards(magnetHand.GetComponent<RectTransform>().transform.position, lowestPoint.GetComponent<RectTransform>().transform.position, 70 * Time.deltaTime);

                        if (hookedObject != null || magnetHand.GetComponent<RectTransform>().transform.position == lowestPoint.GetComponent<RectTransform>().transform.position)
                        {
                            armState = _armState.PullUp;
                        }



                        break;
                    }
                case _armState.PullUp:
                    {


                        magnetHand.GetComponent<RectTransform>().transform.position =
                      Vector3.MoveTowards(magnetHand.GetComponent<RectTransform>().transform.position, this.GetComponent<RectTransform>().transform.position, 70 * Time.deltaTime);

                        if (magnetHand.GetComponent<RectTransform>().transform.position == this.GetComponent<RectTransform>().transform.position)
                            armState = _armState.PullLeft;
                        break;
                    }
                case _armState.PullLeft:
                    {


                        this.GetComponent<RectTransform>().transform.position =
                       Vector3.MoveTowards(this.GetComponent<RectTransform>().transform.position,
                       startPoint.GetComponent<RectTransform>().transform.position, acce * Time.deltaTime);

                        if (magnetHand.GetComponent<RectTransform>().transform.position == startPoint.GetComponent<RectTransform>().transform.position)
                            armState = _armState.PullRelease;
                        break;
                    }
                case _armState.PullRelease:
                    {

                        magnetOn = false;
                        hookedObject = null;
                        armState = _armState.End;

                        break;
                    }
                case _armState.End:
                    {
                        hookedObject = null;

                        if (hookedObject == null)
                        {

                            this.GetComponent<RectTransform>().transform.position =
                        Vector3.MoveTowards(this.GetComponent<RectTransform>().transform.position,
                        centerPoint.GetComponent<RectTransform>().transform.position, 70 * Time.deltaTime);

                            if (this.GetComponent<RectTransform>().transform.position == centerPoint.GetComponent<RectTransform>().transform.position)
                                armState = _armState.NotMovingVertical;

                        }
                        break;
                    }

            }
        }

    }
}
