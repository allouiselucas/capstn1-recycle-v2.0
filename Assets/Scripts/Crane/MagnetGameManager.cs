﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetGameManager : MonoBehaviour
{
    public GameObject magnetArm;
    public GameObject trashPrefab;
    public GameObject trashPrizePrefab;
    public List<GameObject> trashSpawner;
    public List<GameObject> trashList;
    public _gameState gameState;


    public int timesDone;
    public int spawnCount;

    public bool _spawned;
    public bool _gameEnd;

    public _gameDifficulty gameDifficulty;

    public enum _gameDifficulty
    {
        Easy,
        Medium,
        Hard

    }

    public enum _gameState
    {
        NotUse,
        Instruction,
        Start,
        Play,
        Finish
    }

    public void SpawnPrize(GameObject go)
    {
        GameObject trash = Instantiate(go, trashSpawner[0].GetComponent<RectTransform>().transform);
        trash.GetComponent<RectTransform>().transform.position = trashSpawner[Random.Range(1, 3)].GetComponent<RectTransform>().transform.position;
        trashList.Add(trash);
    }

    public void SpawnTrash(GameObject go)
    {
        for (int i = 0; i < spawnCount; i++)
        {
            GameObject trash = Instantiate(go, trashSpawner[0].GetComponent<RectTransform>().transform);
            trash.GetComponent<RectTransform>().transform.position = trashSpawner[Random.Range(0, 3)].GetComponent<RectTransform>().transform.position;
            trashList.Add(trash);
        }
    }

    void Update()
    {
        if (_gameEnd)
        {
            gameState = _gameState.Finish;
        }

        switch (gameState)
        {
            case _gameState.NotUse:

                break;

            case _gameState.Instruction:


                break;

            case _gameState.Start:
                if (!_spawned)
                {
                    if (timesDone == 0)
                    {
                        spawnCount = 10;

                        gameDifficulty = _gameDifficulty.Easy;


                    }
                    if (timesDone == 1)
                    {
                        spawnCount = 15;
                        gameDifficulty = _gameDifficulty.Medium;
                    }
                    if (timesDone >= 2)
                    {
                        spawnCount = 20;
                        gameDifficulty = _gameDifficulty.Hard;
                    }
                    timesDone++;

                    SpawnPrize(trashPrizePrefab);
                    SpawnTrash(trashPrefab);

                    _spawned = true;
                }

                this.GetComponent<RectTransform>().transform.position =
                       Vector3.MoveTowards(this.GetComponent<RectTransform>().transform.position,
                       this.GetComponent<MagnetArmManager>().centerPoint.GetComponent<RectTransform>().transform.position, 70 * Time.deltaTime);
                if (this.GetComponent<RectTransform>().transform.position == this.GetComponent<MagnetArmManager>().centerPoint.GetComponent<RectTransform>().transform.position)
                    gameState = _gameState.Play;


                break;

            case _gameState.Play:

                magnetArm.GetComponent<MagnetArmManager>().operational = true;


                break;

            case _gameState.Finish:

                {
                    foreach (GameObject item in trashList)
                    {

                   
                        
                       
                        Destroy(item);

                    }
                    trashList.Clear();
                    
                    _spawned = false;
                    magnetArm.GetComponent<MagnetArmManager>().operational = false;
                    _spawned = false;
                    
                    spawnCount = trashList.Count;
                    gameState = _gameState.NotUse;
                }
                break;
        }
    }
}

