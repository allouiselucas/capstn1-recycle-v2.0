﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetHandManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.tag == "CraneGamePrize")
            this.GetComponentInParent<MagnetArmManager>().hookedObject = coll.gameObject;
        if (coll.gameObject.tag == "CraneGameTrash")
            this.GetComponentInParent<MagnetArmManager>().hookedObject = coll.gameObject;

    }

    // Update is called once per frame
    void Update () {
		
	}
}
