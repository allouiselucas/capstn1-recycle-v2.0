﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashbinManager : MonoBehaviour
{



    public GameObject magnetArm;

    // Use this for initialization
    void Start()
    {

    }

    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.tag == "CraneGameTrash")
        {

            Destroy(coll.gameObject);
        }
        else if (coll.gameObject.tag == "CraneGamePrize")
        {
            magnetArm.GetComponent<MagnetGameManager>()._gameEnd = true;
        }

    }
}

    
