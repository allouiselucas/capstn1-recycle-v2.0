﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableInventory : MonoBehaviour {

    public GameObject InventoryPanel;

    public void OnClick()
    {
        InventoryPanel.SetActive(!InventoryPanel.activeSelf);
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
