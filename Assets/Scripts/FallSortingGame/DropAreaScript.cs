﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropAreaScript : MonoBehaviour, IDropHandler {

    public FallSortingGameManager GameManager;

    public enum ZoneType
    {
        Biodegradable = 0,
        Non_Biodegradable = 1,
        E_Waste = 2
    }


    public ZoneType _zoneType;


    public void OnDrop(PointerEventData eventData)
    {
        MiniGameTrashBehavior tempTrash = eventData.pointerDrag.GetComponent<MiniGameTrashBehavior>();
        GameObject tempTrashObject = eventData.pointerDrag.gameObject;

        if(tempTrash != null)
        {
            if ((int)tempTrash._trashType == (int)_zoneType)
            {
                Debug.Log("Correct!");
                GameManager.AddScore(5);
                GameManager.spawnedItems.Remove(tempTrashObject);
                Destroy(tempTrashObject);
            }

            else
            {
                Debug.Log("Wrong!");
                GameManager.ReduceScore(5);
                GameManager.spawnedItems.Remove(tempTrashObject);
                Destroy(tempTrashObject);
            }
        }

        else
        {
            Debug.Log("Invalid Move!");
        }

    }


    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}



}
