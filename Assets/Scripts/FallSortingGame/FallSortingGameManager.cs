﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class FallSortingGameManager : MonoBehaviour {


    public GameObject spawnManager;
    public GameObject startPanel;
    public GameObject retryPanel;
    public GameObject mainGamePanel;
    public GameObject miniGameManager;

    public List<GameObject> spawnedItems = new List<GameObject>();

    public GameObject player;
    public int itemID;

    public bool gameStart;
    public bool victory;

    public int _requiredScore;

    public int currentScore;
    public int playerLife;

    public float timeLimit_minutes;
    public float timeLimit_seconds;

    protected float _currentTime;

    protected int _currentScore;
    protected int _playerLife;

    //UI Properties:
    public Text timerText;
    public Text targetScoreText;
    public Text currentScoreText;

    public Text currentPlayerLife;
    TimeSpan timeSpan;

    public float iniSpawnTimeDelay;
    float spawnTimeDelay;
    float curSpawnTimeDelay;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        miniGameManager = GameObject.FindGameObjectWithTag("Mini-Game Manager");
        spawnManager.GetComponent<SpawnerManager>().GameManager = this.GetComponent<FallSortingGameManager>();

        spawnTimeDelay = iniSpawnTimeDelay;
        curSpawnTimeDelay = spawnTimeDelay;
        victory = false;
        _currentTime = (timeLimit_minutes * 60) + timeLimit_seconds;
        _playerLife = 5;
        playerLife = _playerLife;
    }

    public void SetGameVariables(int PlayerLives, int RequiredScore, float MaximumTimeInMinutes, float MaximumTimeInSeconds, float SpawnTimeDelay)
    {
        playerLife = PlayerLives;
        _requiredScore = RequiredScore;
        timeLimit_minutes = MaximumTimeInMinutes;
        timeLimit_seconds = MaximumTimeInSeconds;
        _currentTime = (timeLimit_minutes * 60) + timeLimit_seconds;
        spawnTimeDelay = iniSpawnTimeDelay;
        victory = false;
    }

    void ResetGame()
    {
        currentScoreText.text = "Score: ";
        currentPlayerLife.text = "Life: ";
        for (int i = 0; i < spawnedItems.Count; i++)
        {
            Destroy(spawnedItems[i].gameObject);
        }

        _currentTime = (timeLimit_minutes * 60) + timeLimit_seconds;
        _playerLife = 5;
        playerLife = _playerLife;
        victory = false;
    }

    public void StartGame()
    {
        ResetGame();
        gameStart = true;
        retryPanel.SetActive(false);
        startPanel.SetActive(false);
        mainGamePanel.SetActive(true);
    }

    public void PauseGame()
    {
        gameStart = !gameStart;
    }

    public void EndGame()
    {
        
        ResetGame();
        gameStart = false;
        if (victory == false)
            retryPanel.SetActive(true);

    }

    public void OnVictory()
    {
        ResetGame();
        gameStart = false;
        startPanel.SetActive(true);
        this.gameObject.SetActive(false);
    }

    public void Exit()
    {
        ResetGame();
        gameStart = false;
        startPanel.SetActive(true);
        retryPanel.SetActive(false);
        this.gameObject.SetActive(false);
    }

    public void AddScore(int scorePoint)
    {
        Debug.Log("Check");
        _currentScore += scorePoint;
    }

    public void ReduceScore(int scorePoint)
    {
        _currentScore -=  scorePoint;
        if (_currentScore <= 0) _currentScore = 0;
    }

    public void AddPlayerLife(int lifePoint)
    {
        _playerLife += lifePoint;
    }

    public void ReducePlayerLife(int lifePoint)
    {
        _playerLife -= lifePoint;
        if(_playerLife <= 0)
        {
            victory = false;
            EndGame();
        }
    }




    void Update()
    {
        if(gameStart)
        {
            currentScore = _currentScore;
            UpdateProgress();
            UpdateTimer();
            UpdateUI();
            UpdateSpawn();
        }

    }


    public void UpdateSpawn()
    {
        //this gets called when DoDelay is ready. can instantiate here or whatever.
        if(gameStart)
        {
            curSpawnTimeDelay -= Time.deltaTime;
            if(curSpawnTimeDelay <= 0.0f)
            {
                spawnManager.GetComponent<SpawnerManager>().SetSpawn();
                spawnManager.GetComponent<SpawnerManager>().Spawning();
                curSpawnTimeDelay = spawnTimeDelay;
            }

        }

    }

    void UpdateProgress()
    {
        if(_currentTime > 0 && _currentScore >= _requiredScore)
        {
            victory = true;
            gameStart = false;

            //Show Congratulations
            //Exit Game After Pressing Continue Button
            //For Simplicities Sake for the meantime, Sorting Game Closes After Victory
            player.GetComponent<InventoryUI>().AddItem(itemID);

            OnVictory();
        }

    }

    void UpdateTimer()
    {
        _currentTime -= Time.deltaTime;
        if (_currentTime <= 0.0f)
        {
            victory = false;
            EndGame();
        }
    }

    void UpdateUI()
    {
        timeSpan = TimeSpan.FromSeconds(_currentTime);
        timerText.text = string.Format("{0:D2}:{1:D2}:{2:D2}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
        currentScoreText.text = "Score: " + _currentScore + " / " + _requiredScore;
        currentPlayerLife.text = "Life: " + _playerLife + " / " + playerLife;
    }

}
