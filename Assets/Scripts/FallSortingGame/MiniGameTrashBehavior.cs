﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MiniGameTrashBehavior : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    public GameObject targetObject;
 
    public FallSortingGameManager gameManager;

    public float speed;
    public float speedDamper;

    Vector3 Dir = new Vector3(0, 0, 0);
    Vector3 prevPostion = new Vector3(0,0,0);

    //public string itemName;
    //public string slug;
    public Sprite itemSprite;

    public enum TrashType
    {
        Biodegradable = 1,
        Non_Biodegradable = 2
    }

    public enum ObjectState
    {
        Idle,
        Selected
    }

    public ObjectState curState;
    public TrashType _trashType;

    //Once the object is selected, its previous position prior to selection is saved.
    //id dropped at the right area, add points
    //if the Player drops the item at the wrong area, reduce points
    //if the player drops it back into the panel, it goes back to its original position and continues to head to deadzone

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(gameManager != null)
        {
            prevPostion = this.GetComponent<RectTransform>().position;
            curState = ObjectState.Selected;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
       
    }


    public void OnDrag(PointerEventData eventData)
    {
        if(gameManager != null)
        {
            this.GetComponent<RectTransform>().position = eventData.position;
        }
    }
        


    public void OnEndDrag(PointerEventData eventData)
    {
        //if placed at anywhere but the bins, return to prev position
        if (gameManager != null)
        {
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            this.GetComponent<RectTransform>().position = prevPostion;
            curState = ObjectState.Idle;
        }
       
       
        //if(this.gameObject != null)
        //{

        //}
        //else
        //{

        //}
    }

    // Use this for initialization
    void Start()
    {
        speedDamper = speed;
    }

    // Update is called once per frame
    void Update()
    {
        //If the Current object is not selected, then it continues to move towards the 
        if(gameManager != null)
        {
            
            switch (curState)
            {
                case ObjectState.Idle:
                    {
                        GetComponent<CanvasGroup>().blocksRaycasts = gameManager.gameStart;
                        if (gameManager.gameStart)
                        {

                            Movement();
                        }
                    } break;
                case ObjectState.Selected: { } break;
            }
        }
    }



    public void Movement()
    {
        if (targetObject.gameObject != null)
        {
            DistanceChecker();
            speedDamper += (speedDamper * 0.01f);

            this.gameObject.GetComponent<RectTransform>().position = Vector3.MoveTowards(this.gameObject.GetComponent<RectTransform>().position, targetObject.gameObject.GetComponent<RectTransform>().position, (speed) * Time.deltaTime);
        }

    }

    void DistanceChecker()
    {

        Dir = targetObject.gameObject.GetComponent<RectTransform>().position - this.gameObject.GetComponent<RectTransform>().position;
        Dir.Normalize();

        if (Dir.magnitude <= 0.1f)
        {
            Arrival();
        }
    }


    void Arrival()
    {
        Debug.Log("Miss");
        gameManager.ReducePlayerLife(1);
        gameManager.spawnedItems.Remove(this.gameObject);
        Destroy(this.gameObject);
    }

}
