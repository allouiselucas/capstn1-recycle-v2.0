﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftClass : MonoBehaviour
{
    public int componentID_01 { get; set; }
    public int componentID_02 { get; set; }
    public int craftedID { get; set; }

    public string slug { get; set; }

    public CraftClass(int ComponentID_01, int ComponentID_02, int CraftedID, string Slug)
    {
        componentID_01 = ComponentID_01;
        componentID_02 = ComponentID_02;
        craftedID = CraftedID;

        slug = Slug;
    }

    public CraftClass()
    {
        craftedID = -1;
    }

}
