﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingItemData : MonoBehaviour {

    public ItemClass _item;
    public int slotIndex;
    public int _amount;

    private InventoryUI inventory;
    // Use this for initialization

    public void ResetItemData()
    {
        _item = null;
        slotIndex = -1;
        _amount = 0;
       
    }



}
