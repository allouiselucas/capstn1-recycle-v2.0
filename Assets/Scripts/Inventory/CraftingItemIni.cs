﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingItemIni : MonoBehaviour
{

    public GameObject craftingSlot_01;
    public GameObject craftingSlot_02;
    public GameObject craftedItemSlot;
    public ItemClass craftedItem;

    public GameObject itemDatabase;
    public GameObject player;
    public Sprite[] sprites;


    // Use this for initialization
    void Start()
    {

        player = GameObject.FindGameObjectWithTag("Player");
        itemDatabase = GameObject.FindGameObjectWithTag("ItemDatabase");
        //craftingSlot_01 = craftingSlot_01.transform.GetChild(0).gameObject;
        //craftingSlot_02 = craftingSlot_02.transform.GetChild(0).gameObject;
        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/UI/Inventory_window");

    }

    public void Craft()
    {
        CheckCraftingSlot();
    }

    void CheckCraftingSlot()
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/UI/Inventory_window");

        if (craftingSlot_01.transform.GetChild(0).GetComponent<ItemData>()._item != null && craftingSlot_02.transform.GetChild(0).GetComponent<ItemData>()._item != null)
        {
            CheckCraftdatabase(craftingSlot_01.transform.GetChild(0).GetComponent<ItemData>()._item._iD, craftingSlot_02.transform.GetChild(0).GetComponent<ItemData>()._item._iD);
        }

        else
        {
            Debug.Log("Crafting Failed");
            //prompt: No Recipe exists!
        }

    }

    void ResetCraftingSlots()
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/UI/Inventory_window");
        //craftingSlot_01.transform.GetChild(0).GetComponent<ItemData>()._item = null;
        //craftingSlot_02.transform.GetChild(0).GetComponent<ItemData>()._item = null;

        craftingSlot_01.transform.GetChild(0).GetComponent<Image>().sprite = sprites[0];
        craftingSlot_02.transform.GetChild(0).GetComponent<Image>().sprite = sprites[0];

        craftingSlot_01.transform.GetChild(0).GetComponent<ItemData>().Reset();
        craftingSlot_02.transform.GetChild(0).GetComponent<ItemData>().Reset();
    }

    void CheckCraftdatabase(int SlotID_01, int SlotID_02)
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/UI/Inventory_window");

        //for (int i = 0; i < itemDatabase.GetComponent<ItemDatabase>().craftItemDatabase.Count; i++)
        //{
        //    if ((SlotID_01 == itemDatabase.GetComponent<ItemDatabase>().craftItemDatabase[i].componentID_01 ||
        //        SlotID_01 == itemDatabase.GetComponent<ItemDatabase>().craftItemDatabase[i].componentID_02) &&
        //        (SlotID_02 == itemDatabase.GetComponent<ItemDatabase>().craftItemDatabase[i].componentID_01 ||
        //        SlotID_02 == itemDatabase.GetComponent<ItemDatabase>().craftItemDatabase[i].componentID_02))
        //    {
        //        Debug.Log(SlotID_01 + " " + SlotID_02);
        //        Debug.Log("Craft Success!");
        //        int tempID = itemDatabase.GetComponent<ItemDatabase>().craftItemDatabase[i].craftedID;
        //        craftedItem = itemDatabase.GetComponent<ItemDatabase>().dataBase[tempID];
        //        craftedItemSlot.transform.GetChild(0).GetComponent<Image>().sprite = itemDatabase.GetComponent<ItemDatabase>().dataBase[tempID]._ItemSpriteImage;

        //        //craftingSlot_01.GetComponent<CraftingItemData>().ResetItemData();
        //        //craftingSlot_02.GetComponent<CraftingItemData>().ResetItemData();

        //        craftingSlot_01.transform.GetChild(0).GetComponent<Image>().sprite = null;
        //        craftingSlot_02.transform.GetChild(0).GetComponent<Image>().sprite = null;

        //        player.GetComponent<InventoryUI>().AddItem(tempID);
        //    }
        //}

        if ((SlotID_01 == 0 || SlotID_02 == 0) && (SlotID_02 == 2 || SlotID_01 == 2))
        {
            ResetCraftingSlots();
            player.GetComponent<InventoryUI>().AddItem(7);
        }

        else if ((SlotID_01 == 1 || SlotID_02 == 1) && (SlotID_01 == 7 || SlotID_02 == 7))
        {
            ResetCraftingSlots();
            player.GetComponent<InventoryUI>().AddItem(10);
        }

        else if ((SlotID_01 == 3 || SlotID_02 == 3) && (SlotID_01 == 4 || SlotID_02 == 4))
        {
            ResetCraftingSlots();
            player.GetComponent<InventoryUI>().AddItem(8);
        }

        else
        {
            Debug.Log("Crafting Recipe does not exist" + SlotID_01 + SlotID_02);
            if (craftingSlot_01.transform.GetChild(0).GetComponent<ItemData>()._item._iD != -1 && craftingSlot_02.transform.GetChild(0).GetComponent<ItemData>()._item._iD != -1)
            {
                player.GetComponent<InventoryUI>().AddItem(craftingSlot_01.transform.GetChild(0).GetComponent<ItemData>().slotIndex);
                player.GetComponent<InventoryUI>().AddItem(craftingSlot_02.transform.GetChild(0).GetComponent<ItemData>().slotIndex);
                ResetCraftingSlots();
            }
        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
