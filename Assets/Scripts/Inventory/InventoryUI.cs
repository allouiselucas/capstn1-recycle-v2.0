﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{


    public GameObject _player;
    public Inventory _playerInventory;
    public List<ItemClass> _playerItemInventory;

    public GameObject _inventoryItem;
    public GameObject _inventoryPanel;
    public GameObject _slot;
    public GameObject _slotPanel;

    public List<ItemClass> _items = new List<ItemClass>();
    public List<GameObject> _slots = new List<GameObject>();

    public ItemDatabase _dataBase;
    private int _slotCount;

    // Use this for initialization
    void Start()
    {
        if (_player == null)
        {
            _player = GameObject.FindGameObjectWithTag("Player");
            if (_player.GetComponent<Inventory>() != null)
            {
                _playerInventory = _player.GetComponent<Inventory>();
                _playerItemInventory = _playerInventory._itemList;
            }
        }
        _dataBase = GameObject.FindGameObjectWithTag("ItemDatabase").GetComponent<ItemDatabase>();

        _slotCount = 15;
        // = GameObject.Find("Inventory Panel");
        _slotPanel = _inventoryPanel.transform.FindChild("Slot Panel").gameObject;


        for (int i = 0; i < _slotCount; i++)
        {
            _items.Add(new ItemClass());
            _slots.Add(Instantiate(_slot));
            _slots[i].GetComponent<SlotScript>()._id = i;
            //_slots[i].transform.GetComponent<Image>().sprite = Resources.Load("unity_builtin_extra/Background", typeof(Sprite)) as Sprite;
            Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/UI/Inventory_window");
            _slots[i].transform.GetComponent<Image>().sprite = sprites[0];
            _slots[i].transform.SetParent(_slotPanel.transform);
        }

        //For testing purposes

        for (int i = 0; i < 6; i++)
        {
            AddItem(i);
            AddItem(i);
        }


    }


    public void Removeitem(int id)
    {

    }
    public void AddItem(int id)
    {
        ItemClass itemToAdd = _dataBase.FetchItemByID(id);
        if (itemToAdd._stackable && CheckItemInventory(itemToAdd))
        {
            for (int i = 0; i < _slots.Count; i++)
            {
                if(_slots[i].transform.childCount != 0)
                {
                    if (_items[i]._iD == itemToAdd._iD)
                    {
                        ItemData data = _slots[i].transform.GetChild(0).GetComponent<ItemData>();
                        data._amount++;
                        data.transform.GetChild(0).GetComponent<Text>().text = data._amount.ToString();
                        Debug.Log(data._item._name);
                        break;
                    }
                }

                else
                {
                    _items[i] = itemToAdd;
                    GameObject itemObj = Instantiate(_inventoryItem);
                    itemObj.GetComponent<ItemData>()._item = itemToAdd;
                    itemObj.GetComponent<ItemData>().slotIndex = i;

                    itemObj.transform.SetParent(_slots[i].transform, false);
                    itemObj.GetComponent<Image>().sprite = itemToAdd._ItemSpriteImage;
                    itemObj.transform.position = _slots[i].transform.position;
                    //itemObj.transform.position = Vector2.zero;
                    itemObj.name = itemToAdd._name;

                    ItemData data = _slots[i].transform.GetChild(0).GetComponent<ItemData>();
                    data._amount++;
                    data.transform.GetChild(0).GetComponent<Text>().text = data._amount.ToString();
                    //Debug.Log("Check");
                    break;
                }
                
            }

        }

        else
        {
            Debug.Log("Called");
            for (int i = 0; i < _items.Count; i++)
            {
                if (_items[i]._iD == -1)
                {
                    _items[i] = itemToAdd;
                    GameObject itemObj = Instantiate(_inventoryItem);
                    itemObj.GetComponent<ItemData>()._item = itemToAdd;
                    itemObj.GetComponent<ItemData>().slotIndex = i;

                    itemObj.transform.SetParent(_slots[i].transform, false);
                    itemObj.GetComponent<Image>().sprite = itemToAdd._ItemSpriteImage;
                    itemObj.transform.position = _slots[i].transform.position;
                    //itemObj.transform.position = Vector2.zero;
                    itemObj.name = itemToAdd._name;

                    ItemData data = _slots[i].transform.GetChild(0).GetComponent<ItemData>();
                    data._amount++;
                    data.transform.GetChild(0).GetComponent<Text>().text = data._amount.ToString();
                    //Debug.Log("Check");
                    break;
                }

                else
                {

                }

            }
        }
    }

    //
    public bool CheckItemInventory(ItemClass Item)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i]._iD == Item._iD)
            {
                return true;
            }
        }

        return false;
    }


}
