﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemClass
{
    public string _name { get; set; }
    public string _itemDescription { get; set; }
    public int _iD { get; set; }

    public string _slug { get; set; }

    public enum ItemType
    {
        Non_Quest = 0,
        Quest = 1
    }

    public ItemType _itemType { get; set; }
    public int _itemTypeInt { get; set; }

    public bool _stackable { get; set; }
    public Sprite _ItemSpriteImage { get; set; }


    public ItemClass(int ID, string Name, string Description, int ItemType, bool Stackable, string Slug)
    {
        this._name = Name;
        this._iD = ID;
        this._itemDescription = Description;
        this._stackable = Stackable;
        this._slug = Slug;

        this._ItemSpriteImage = Resources.Load<Sprite>("Sprites/Items/" + _slug);
        //Debug.Log(this._slug);

        //Checks for Enum Value if Input is within parameter range 
        for (int i = 0; Enum.GetValues(typeof(ItemType)).Length < i; i++)
        {
            if (ItemType == i)
            {
                _itemType = (ItemType)ItemType;
            }

            else { Debug.Log(ItemType + " value is not valid!"); }
        }

    }

    //Constructor for emptying slot item or for testing errors.
    //This creates an empty item.
    public ItemClass()
    {
        this._iD = -1;
    }


}
