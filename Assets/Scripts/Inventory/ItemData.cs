﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemData : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    //This will handle drag and drop

    public ItemClass _item;
    public int slotIndex;
    public int _amount;

    private Transform originalParent;
    private Vector3 originalPosition;
    private InventoryUI player;

    public Text amountText;

    public void Reset()
    {
        _item = new ItemClass();
        slotIndex = 0;
        _amount = 0;

    }
    void Start()
    {
        amountText = this.transform.GetChild(0).GetComponent<Text>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<InventoryUI>();
        Debug.Log(_item._name + _item._iD);
    }

    
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (_item != null)
        {
            originalParent = this.transform.parent;
            originalPosition = this.transform.position;
            this.GetComponent<CanvasGroup>().blocksRaycasts = false;

            this.transform.SetParent(originalParent.parent.parent.parent);
            this.transform.position = eventData.position;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (_item != null)
        {

            this.transform.position = eventData.position;
        }

    }


    public void OnEndDrag(PointerEventData eventData)
    {
        //Create conditional if the item is not dropped in proper area.
        //this.transform.SetParent(originalParent);
        //this.transform.position = originalPosition;
        if (this.gameObject.tag != "Crafting Slot")
        {
            if(_amount != 0)
            {
                this.transform.SetParent(player._slots[slotIndex].transform);
                this.transform.position = player._slots[slotIndex].transform.position;
            }

            else
            {
                Destroy(this.gameObject);
            }
            //Debug.Log(_item._name + _item._iD);

        }


        this.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }


    void Update()
    {
        amountText.text = _amount.ToString();
    }
}
