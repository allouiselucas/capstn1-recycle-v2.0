﻿    using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using LitJson;

public class ItemDatabase : MonoBehaviour
{

    public List<GameObject> _items;
    public  List<ItemClass> dataBase = new List<ItemClass>();
    public  List<CraftClass> craftItemDatabase = new List<CraftClass>();

    private JsonData _itemData;
    private JsonData _itemCraftData;


    void Start()
    {
        _itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));

        //_itemCraftData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/CraftingRecipe.json"));

        ConstructItemDatabase();

        // ConstructCraftItemDatabase();
        //Debug.Log(dataBase[0]._name);
        //Debug.Log(FetchItemByID(3)._itemDescription);
        //Debug.Log(dataBase.Count);
    }

    void ConstructCraftItemDatabase()
    {
        for (int i = 0; i < _itemCraftData.Count; i++)
        {
            craftItemDatabase.Add(new CraftClass((int)_itemCraftData[i]["componentID_01"], (int)_itemCraftData[i]["componentID_02"], 
                (int)_itemCraftData[i]["craftedID"], _itemCraftData[i]["name"].ToString()));
        }
    }

    void ConstructItemDatabase()
    {
        for (int i = 0; i < _itemData.Count; i++)
        {
            dataBase.Add(new ItemClass((int)_itemData[i]["id"], _itemData[i]["name"].ToString(), _itemData[i]["description"].ToString(),
                (int)_itemData[i]["typeID"], (bool)_itemData[i]["stackable"], _itemData[i]["slug"].ToString()));
        }
    }


    public ItemClass FetchItemByID(int ID)
    {
        for (int i = 0; i < dataBase.Count; i++)
        {
            if (dataBase[i]._iD == ID)
            {
                return dataBase[i];
            }
            
        }

        return null;
    }



}


