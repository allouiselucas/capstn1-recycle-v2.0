﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;
using System;

public class SlotScript : MonoBehaviour, IDropHandler
{
    //public ItemClass item;
    //Image itemImage;
    //public int capacity;
    //public int slotIndex;
    //public Text descriptionText;
    //public Text ItemAmount;

    private InventoryUI player;
    public int _id;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<InventoryUI>();
    }




    public void OnDrop(PointerEventData eventData)
    {
        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();
        //Debug.Log("Dropped to:  " + gameObject.name);

        if(gameObject.tag == "Crafting Slot")
        {
            if(droppedItem.slotIndex != gameObject.transform.GetChild(0).GetComponent<ItemData>().slotIndex)
            {
                if(gameObject.transform.GetChild(0).GetComponent<ItemData>()._item != null)
                {
                    player.AddItem(gameObject.transform.GetChild(0).GetComponent<ItemData>().slotIndex);
                }
                

                gameObject.transform.GetChild(0).GetComponent<Image>().sprite = droppedItem.GetComponent<Image>().sprite;
                gameObject.transform.GetChild(0).GetComponent<ItemData>().slotIndex = droppedItem.slotIndex;
                gameObject.transform.GetChild(0).GetComponent<ItemData>()._item = droppedItem._item;
                gameObject.transform.GetChild(0).GetComponent<ItemData>()._amount = 1;
                droppedItem._amount -= 1;
            }

            else if(droppedItem.slotIndex == gameObject.transform.GetChild(0).GetComponent<ItemData>().slotIndex)
            {
                if(droppedItem.slotIndex != 0 && gameObject.transform.GetChild(0).GetComponent<ItemData>().slotIndex != 0)
                {
                    gameObject.transform.GetChild(0).GetComponent<Image>().sprite = droppedItem.GetComponent<Image>().sprite;
                    gameObject.transform.GetChild(0).GetComponent<ItemData>().slotIndex = droppedItem.slotIndex;
                    gameObject.transform.GetChild(0).GetComponent<ItemData>()._item = droppedItem._item;
                    gameObject.transform.GetChild(0).GetComponent<ItemData>()._amount = 1;
                }

                else if(droppedItem.slotIndex == 0 && gameObject.transform.GetChild(0).GetComponent<ItemData>().slotIndex == 0 && gameObject.transform.GetChild(0).GetComponent<ItemData>()._amount == 0)
                {
                    gameObject.transform.GetChild(0).GetComponent<Image>().sprite = droppedItem.GetComponent<Image>().sprite;
                    gameObject.transform.GetChild(0).GetComponent<ItemData>().slotIndex = droppedItem.slotIndex;
                    gameObject.transform.GetChild(0).GetComponent<ItemData>()._amount = 1;
                    gameObject.transform.GetChild(0).GetComponent<ItemData>()._item = droppedItem._item;
                    droppedItem._amount -= 1;
                }



            }

            if (droppedItem._amount == 0 && this.gameObject.tag != "Crafting Slot")
            {
                Debug.Log("Remove: " + droppedItem.name + " " + droppedItem.slotIndex);
                //_id = 0;
                player._slots.RemoveAt(droppedItem.slotIndex);
                //player._items[droppedItem.slotIndex] = new ItemClass();
                player._items[droppedItem.slotIndex] = new ItemClass() ;
            }

            //Send Data to gameObject: ItemID and deduct 1 from ItemDropped.
            //If Dropped and Slot is occupied, Add Previous item back into Inventory. Replace previous item afterwards with new one.
            //If Slot is clocked and not dragged or dragged outside, return item to inventory and leave slot empty again
        }

        else if (droppedItem.tag != "Crafting Slot" && player._items[_id]._iD == -1)
        {
            //Debug.Log("Drop to:  " + gameObject.name);
            //inventory._items[droppedItem.slotIndex] = new ItemClass();
            //droppedItem.transform.SetParent(this.transform);
            //droppedItem.transform.position = this.transform.position;
            player._items[droppedItem.slotIndex] = new ItemClass();
            droppedItem.slotIndex = _id;
        }

       else if (droppedItem.slotIndex != _id)
        {
            //int tempIndexDrop = droppedItem.slotIndex;
            //int tempIndexDropOn = gameObject.GetComponent<SlotScript>()._id;

            //gameObject.GetComponent<SlotScript>()._id = droppedItem.slotIndex;
            //droppedItem.slotIndex = tempIndexDropOn;

            Transform item = this.transform.GetChild(0);
            item.GetComponent<ItemData>().slotIndex = droppedItem.slotIndex;
            item.transform.SetParent(player._slots[droppedItem.slotIndex].transform);
            item.transform.position = player._slots[droppedItem.slotIndex].transform.position;

            droppedItem.slotIndex = _id;
            droppedItem.transform.SetParent(this.transform);
            droppedItem.transform.position = this.transform.position;

            //player._items[droppedItem.slotIndex] = item.GetComponent<ItemData>()._item;
            //player._items[_id] = droppedItem._item;

        }


    }
}
