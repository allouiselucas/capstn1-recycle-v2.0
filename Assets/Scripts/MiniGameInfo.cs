﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MiniGameInfo : MonoBehaviour
{
    //Holds all necessary Mini-Game Info.
    public bool isEmpty;
    public ItemClass item =  new ItemClass();
    public int itemID;
    public enum MiniGameType
    {
        DoubleSort,
        TripleSort,
        Crane
    }

    public enum MiniGameDifficulty
    {
        Easy,
        Fair,
        Hard,
        Challenging
    }

    public MiniGameType _miniGameType;
    public MiniGameDifficulty _miniGameDifficulty;

    void Start()
    {
        itemID = item._iD;
    }

    public void SetItem(ItemClass Item)
    {
        item = Item;
        CheckIfEmpty();
    }

    void CheckIfEmpty()
    {
        if (isEmpty)
        {
            this.transform.parent.GetChild(1).gameObject.SetActive(false);
        }
        else
        {
            this.transform.parent.GetChild(1).gameObject.SetActive(true);
        }
    }
    void Update()
    {
        //Revise later
        
    }

}


