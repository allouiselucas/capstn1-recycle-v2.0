﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameManager : MonoBehaviour
{
    //Minigame trigger calls a function in Mini-Game to set and bring up the mini-game event.
    //Minigame manager handles the setting of the Mini-Game variables such as Time Limit, Rate of Spawn and Points needed to complete minigame.
    //Minigame manager also handles the adding of item to the player's inventory after successful completion of minigame.
    //Minigame manager also removes 'marker' for minigame after it is finished.
    public enum MiniGameType
    {
        DoubleSort,
        TripleSort,
        Crane
    }

    public enum MiniGameDifficulty
    {
        Easy,
        Fair,
        Hard,
        Challenging
    }

    public MiniGameType _miniGameType;
    public MiniGameDifficulty _miniGameDifficulty;

    public GameObject miniGamePanel;

    public GameObject miniSortGamePanel;
    public GameObject miniConveyerGamePanel;

    public int playerLives;
    public int requiredScore;

    public float timeInMinutes;
    public float timeInSeconds;

    //SpawnDelay are Set. Not Random
    public float spawnTimeDelay;

    private bool isEmpty;
    public int itemID;

    //Can't think of a proper name yet....yet.
    void Randomizer(int MinPlayerLives, int MaxPlayerLives, int MinRequiredScore,
        int MaxRequiredScore, int MinTimeInMinute, int MaxTimeInMinute, int MinTimeInSeconds,
        int MaxTimeInSeconds, float MinSpawnDelay, float MaxSpawnDelay)
    {
        playerLives = Random.Range(MinPlayerLives, MaxPlayerLives);
        requiredScore = Random.Range(MinRequiredScore, MaxRequiredScore);
        timeInMinutes = Random.Range(MinTimeInMinute, MaxTimeInMinute);
        timeInSeconds = Random.Range(MinTimeInSeconds, MaxTimeInSeconds);
    }

    void Start()
    {

    }

    public void EmptyPile(bool IsEmpty)
    {
        isEmpty = IsEmpty;
    }

    public void SetMiniGameParameter(int MGType, int MGDifficulty, ItemClass Item)
    {
        _miniGameType = (MiniGameType)MGType;
        _miniGameDifficulty = (MiniGameDifficulty)MGDifficulty;
        itemID = Item._iD;
        StartGameProper();
    }

    public void SetGameDifficulty()
    {
        switch (_miniGameDifficulty)
        {
            //Create Random Variables
            case MiniGameDifficulty.Easy:
                {
                    Randomizer(5, 5, 50, 75, 1, 2, 20, 30, 2.0f, 1.75f);
                    break;
                }

            case MiniGameDifficulty.Fair:
                {
                    Randomizer(4, 5, 65, 100, 1, 1, 0, 20, 1.75f, 1.5f);
                    break;
                }

            case MiniGameDifficulty.Hard:
                {
                    Randomizer(3, 5, 80, 115, 1, 1, 0, 300, 1.25f, 1.0f);
                    break;
                }

            case MiniGameDifficulty.Challenging:
                {
                    Randomizer(2, 3, 100, 150, 1, 1, 0, 0, 1.0f, 0.85f);
                    break;
                }
        }

    }

    void SetGameType()
    {
        //MiniGame Type
        switch (_miniGameType)
        {
            case MiniGameType.DoubleSort:
                {
                    itemID = Random.Range(0, 3);
                    miniSortGamePanel.GetComponent<FallSortingGameManager>().SetGameVariables(playerLives, requiredScore, timeInMinutes, timeInSeconds, spawnTimeDelay);
                    miniSortGamePanel.GetComponent<FallSortingGameManager>().itemID = itemID;
                    miniSortGamePanel.SetActive(true);
                    break;
                }

            case MiniGameType.TripleSort:
                {
                    itemID = Random.Range(0, 3); //This is only temporary
                    miniSortGamePanel.GetComponent<FallSortingGameManager>().SetGameVariables(playerLives, requiredScore, timeInMinutes, timeInSeconds, spawnTimeDelay);
                    miniSortGamePanel.GetComponent<FallSortingGameManager>().itemID = itemID;
                    miniConveyerGamePanel.SetActive(true);
                    break;
                }

            case MiniGameType.Crane:
                {

                    break;
                }

        }
    }
    void StartGameProper()
    {

        if (isEmpty != true)
        {
            SetGameDifficulty();
            SetGameType();
            
        }
    }

    void Update()
    {

    }
}
