﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniGameTrigger : MonoBehaviour {

    

    public GameObject interactPrompt;
    public GameObject emptyPrompt;

    public GameObject interactRadial;
    public GameObject miniGamePanel;
    
    public GameObject miniGameManager;

    public bool _canInteract;

    public float holdTime = 1.0f;
    public float curHoldTime;

    void Start()
    {
        curHoldTime = holdTime;
        miniGameManager = GameObject.FindGameObjectWithTag("Mini-Game Manager");
        //emptyPrompt = GameObject.FindGameObjectWithTag("Empty Prompt");
        //interactPrompt = GameObject.FindGameObjectWithTag("Interact Prompt");
        //interactRadial = interactPrompt.transform.GetChild(0).gameObject;
    }

    void OnTriggerEnter(Collider objectCollided)
    {
        if(objectCollided.gameObject.tag == "Player")
        {
            _canInteract = true;
            Debug.Log("Player has entered collision Area");
            interactPrompt.SetActive(true);
        }

        //Pop-Up Interact Prompt
        //Hold E to Interact
        //Pop-Up Mini-Game Panel

    }

    void OnTriggerExit(Collider objectCollided)
    {
        if (objectCollided.gameObject.tag == "Player")
        {
            _canInteract = false;
            interactPrompt.SetActive(false);
        }

            //If player exits trigger, Interact Prompt disappears.
    }

    void Interact()
    {
        //if(_canInteract == true && miniGamePanel.activeSelf == false)
        if (_canInteract == true)
        {
            if(Input.GetKey(KeyCode.T))
            {
                curHoldTime -= Time.deltaTime;

                interactRadial.GetComponent<Image>().fillAmount += Time.deltaTime ;

                if(curHoldTime <= 0.0f)
                {
                    //Debug.Log("Mini-Game Start!");
                    curHoldTime = holdTime;
                    interactRadial.GetComponent<Image>().fillAmount = 0;

                    //Set Minigame stat parameters here
                    InitializeMiniGames();

                    //miniGamePanel.SetActive(true);
                    interactPrompt.SetActive(false);
                    //Prompt Mini-Game
                    //If MiniGame is already prompted, cannot re-prompt
                }
            }

            else
            {
                curHoldTime = holdTime;
                if(interactRadial.GetComponent<Image>().fillAmount != 0)
                {
                    interactRadial.GetComponent<Image>().fillAmount -= Time.deltaTime;
                }
            }
        }

    }

    void InitializeMiniGames()
    {
        if(this.GetComponent<MiniGameInfo>().isEmpty != true)
        {
            miniGameManager.GetComponent<MiniGameManager>().SetMiniGameParameter((int)this.GetComponent<MiniGameInfo>()._miniGameType,
            (int)this.GetComponent<MiniGameInfo>()._miniGameDifficulty, this.GetComponent<MiniGameInfo>().item);
            
        }

        else
        {
            miniGameManager.GetComponent<MiniGameManager>().EmptyPile(this.GetComponent<MiniGameInfo>().isEmpty);
            emptyPrompt.SetActive(true);
            interactPrompt.SetActive(false);
        }
        
    }



	void Update ()
    {
        Interact();
    }
}
