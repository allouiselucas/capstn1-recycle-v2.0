﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixingButton : MonoBehaviour
{

    public Collider2D circleCollider;
    public MixingManager mixingManager;

    public int clickCount;

    public void Click()
    {
        OnTriggerEnter2D(circleCollider);
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if(mixingManager.GetComponent<MixingManager>().gameState == MixingManager._gameState.Play)
        if (coll.gameObject == circleCollider)
        {
            mixingManager.GetComponent<MixingManager>().checker[clickCount].GetComponent<CheckerManager>().checkerState = CheckerManager._checkerState.Correct;
           
        }
        else
        {
            mixingManager.GetComponent<MixingManager>().checker[clickCount].GetComponent<CheckerManager>().checkerState = CheckerManager._checkerState.Wrong;
            
        }

        clickCount++;

        mixingManager.GetComponent<MixingManager>().AnswerCount();

    }
}
