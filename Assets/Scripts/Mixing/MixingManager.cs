﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class MixingManager : MonoBehaviour
{

    public float countDownTimer;
    public float acceleration;
    public float speed;

    public int rotationCount;
    public int correctAnswerCount;
    public int wrongAnswerCount;

    public bool isCollided;

    public GameObject startButton;
    public GameObject instructions;
    public GameObject circle;


    public List<GameObject> checker;



    public Text countDownText;

    private Vector3 rotationEuler;
    private Vector3 startingPoint;


    float deltaRotation;
    float currentRotation;
    float WindupRotation;



    public _gameState gameState;
    public enum _gameState
    {
        NotUse,
        Instruction,
        Start,
        Play,
        Finish
    }

    public void NotUse()
    {
        this.gameObject.SetActive(false);
    }

    public void Instruction()
    {
        this.gameObject.SetActive(true);
        instructions.SetActive(true);
        circle.SetActive(false);
        startButton.SetActive(true);
        foreach (GameObject c in checker)
        {
            c.GetComponent<CheckerManager>().checkerState = CheckerManager._checkerState.Unused;
        }
        countDownText.gameObject.SetActive(false);


        deltaRotation = 0;
        currentRotation = 0;
        WindupRotation = 0;
        
    }

    public void StartButton()
    {
        countDownTimer = 5.0f;

        startButton.SetActive(false);
        instructions.SetActive(false);
        circle.SetActive(true);

        foreach (GameObject c in checker)
        {
            c.SetActive(true);

        }

        startingPoint.z = circle.transform.rotation.z;

        countDownText.gameObject.SetActive(true);



        gameState = _gameState.Start;
    }

    public void AnswerCount()
    {
        if (gameState == _gameState.Play)
        {


            correctAnswerCount = 0;
            wrongAnswerCount = 0;

            foreach (GameObject go in checker)
            {
                if (go.GetComponent<CheckerManager>().checkerState == CheckerManager._checkerState.Correct)
                {
                    correctAnswerCount++;
                }

                if (go.GetComponent<CheckerManager>().checkerState == CheckerManager._checkerState.Wrong)
                {
                    wrongAnswerCount++;
                }
            }
            if (correctAnswerCount >= 3)
            {
                //win
                gameState = _gameState.Finish;
            }

            else if (wrongAnswerCount >= 3)
            {
                //lose
                gameState = _gameState.Finish;
            }
        }

    }



    public void Update()
    {


        if (gameState == _gameState.Instruction)
        {
            Instruction();
        }

        if (instructions.activeSelf != true)
        {

            if (countDownTimer > 0.0 && gameState == _gameState.Start)
            {
                countDownTimer -= Time.deltaTime;

                int temp = (int)countDownTimer + 1;

                countDownText.text = temp.ToString();
            }

            else if (countDownTimer <= 0.0 && gameState == _gameState.Start)
            {
                countDownText.gameObject.SetActive(false);
                gameState = _gameState.Play;
            }

            if (gameState == _gameState.Play && rotationCount <= 10)
            {
                if (rotationCount <= 10)
                {
                    deltaRotation = (currentRotation - circle.transform.eulerAngles.z);
                    currentRotation = circle.transform.eulerAngles.z;
                    if (deltaRotation >= 300)
                        deltaRotation -= 360;
                    if (deltaRotation <= -300)
                        deltaRotation += 360;
                    WindupRotation += (deltaRotation);

                    rotationCount = (int)WindupRotation / 360 * -1;
                }

                rotationEuler += Vector3.forward * 30 * (rotationCount + 1) * Time.deltaTime;
                circle.transform.rotation = Quaternion.Euler(rotationEuler);
            }
        }


    }
}
