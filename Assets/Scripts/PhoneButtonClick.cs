﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneButtonClick : MonoBehaviour {

    public GameObject PhoneCanvas;
    public GameObject PhonePanel;

    public List<GameObject> thingsToHide;
    public List<GameObject> thingsToShow;

    public void OnClick()
    {
        foreach (GameObject go in thingsToHide)
        {
            go.SetActive(false);
        }

        foreach (GameObject go in thingsToShow)
        {
            go.SetActive(true);
        }

        PhonePanel.SetActive(!PhonePanel.activeSelf);

    }
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
