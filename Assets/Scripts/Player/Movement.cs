﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Movement : MonoBehaviour
{
    UnityEngine.AI.NavMeshAgent agent;
    public float _distance;
    public float _arriveDist = 0.5f;
    public float speed;
    Animator anim;
    Vector3 Dir;

    Camera mainCamera;

    void Start()
    {
        mainCamera = Camera.main;
        if (speed == 0) speed = 3.0f;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        anim = GetComponent<Animator>();

    }

    public void UpdateMovement()
    {
        //Forward
        if (Input.inputString == "W")
        {
            //Debug.Log("Check");
            
            transform.forward += new Vector3(speed * Time.deltaTime, transform.position.y, transform.position.z);
        }
        //Backward
        if (Input.inputString == "S")
        {
            transform.forward += new Vector3(-(speed * Time.deltaTime), transform.position.y, transform.position.z);
        }
        //Left
        if (Input.inputString == "A")
        {
            transform.right += new Vector3(transform.position.x, transform.position.y, -speed * Time.deltaTime);
            mainCamera.transform.RotateAround(this.transform.position, Vector3.up, 90 * Time.deltaTime);
        }
        //Right
        if (Input.inputString == "D")
        {
            transform.right += new Vector3(transform.position.x, transform.position.y, speed * Time.deltaTime);
            mainCamera.transform.RotateAround(this.transform.position, Vector3.up, -90 * Time.deltaTime);
        }
    }

    //public void UpdateMovement(Vector3 targetPoint)
    //{
    //    Dir = targetPoint - this.transform.position;
    //    Dir.Normalize();
    //   // _distance = Dir.magnitude;

    //    _distance = Vector3.Distance(this.transform.position, targetPoint);

    //    if (_distance >= _arriveDist)
    //    {
    //        //var rot = Quaternion.LookRotation(Dir);
    //        //transform.rotation = Quaternion.Slerp(transform.rotation, rot, 45.0f * Time.deltaTime);

    //        //this.transform.position = new Vector3(transform.forward.x, 0, transform.forward.z);
    //        //this.transform.position += this.transform.position * speed;
    //        //this.transform.position.y -= -9.8 * Time.deltaTime;
    //        //agent.destination = targetPoint;

    //        // Dir = targetPoint - this.transform.position;
    //        //_distance = Dir.magnitude;

    //        // transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Dir), 25.0f * Time.deltaTime);
    //        //this.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, targetPoint, (speed * Time.deltaTime));
    //        agent.SetDestination(targetPoint);



    //        //agent.updateRotation = false;
    //        anim.SetBool("isWalking", true);

    //    }

    //    else if (_distance <= _arriveDist)
    //    {
    //        this.gameObject.GetComponent<PlayerController>().ChangeState(0);
    //        this.gameObject.GetComponent<PlayerController>()._isIdle = true;
    //        anim.SetBool("isWalking", false);

    //    }


    //}


}
