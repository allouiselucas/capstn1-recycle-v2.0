﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{

    Animator anim;
    Movement movement;

    Vector3 targetPoint;
    GameObject targetObject;
    public GameObject SelectedItem;
    float timeCounter;

    public bool _isIdle;
    public float movementSpeed = 5f;
    public float cameraSmoothSpeed = 90.0f;
    public float speed;

    public GameObject marker;
    public Camera mainCamera;

    Vector3 movementVector;

    public enum _State
    {
        Idle = 0,
        Moving = 1,
    }

    public _State _curState;

    void Start()
    {
        anim = GetComponent<Animator>();
        if (speed == 0) speed = 2f;
        movement = this.GetComponent<Movement>();
        _curState = _State.Idle;

        mainCamera = Camera.main;

        GameObject marker_02 = Instantiate(marker, this.transform.position, Quaternion.identity) as GameObject;
        marker_02.transform.parent = this.transform;
        marker_02.transform.position = new Vector3(marker_02.transform.parent.position.x, marker_02.transform.parent.position.y + 5, marker_02.transform.parent.position.z);

        Quaternion markerRotation = new Quaternion();
        markerRotation.eulerAngles = new Vector3(90, -45, -45);
        marker_02.transform.localRotation = markerRotation;
    }

    public void ChangeState(int state)
    {
        _curState = (_State)state;
    }


    //Revise
    void PlayerMouseInput()
    {
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {

            }
        }



    }

    void UpdateIdle()
    {
        if (_isIdle == true) { /*Animation for Idle*/ _isIdle = false; }
    }

    void PlayerKeyBoardInput()
    {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //Debug.Log(moveHorizontal);

        //if (moveHorizontal > 0.0f || moveHorizontal < 0.0f)
        //{
        //    mainCamera.GetComponent<cameraBehavior>().RotateCamera(moveHorizontal);
        //}


        if (moveHorizontal != 0f || moveVertical != 0f)
        {
            if (movementSpeed > 0)
            {
                anim.SetBool("isWalking", true);
                movementVector = new Vector3(moveHorizontal, transform.position.y, moveVertical);
                movementVector = Camera.main.transform.TransformDirection(movementVector);
                movementVector.y = 0.0f;

                Quaternion newForwardRotation = Quaternion.LookRotation(movementVector, Vector3.up);
                Quaternion newRotation = Quaternion.Lerp(this.transform.rotation, newForwardRotation, cameraSmoothSpeed * Time.deltaTime);

                transform.rotation = newRotation;
                transform.position += movementVector * movementSpeed * Time.deltaTime;
            }
        }

        else
        {
            _isIdle = true;
            anim.SetBool("isWalking", false);
        }

    }

    // Update is called once per frame
    void Update()
    {
        PlayerMouseInput();
        PlayerKeyBoardInput();
        switch (_curState)
        {
            case _State.Idle: UpdateIdle(); break;
                //case _State.Moving: movement.UpdateMovement(targetPoint); break;
                //case _State.Moving: movement.UpdateMovement(); break;

        }
    }
}