﻿using UnityEngine;
using System.Collections;

public class cameraBehavior : MonoBehaviour
{
   // private Transform myPos;
    private float camX, camY, camZ, playX, playY, playZ;
    //private float distDamping;
    private Vector3 cameraPos, playerPos;
    private Vector3 lookPlayer;
    GameObject player;
    float timeCounter;

    Vector3 _currentOffset;
    Vector3 _initialOffset;



    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //myPos = GetComponent<Transform>();
       // distDamping = 6f;
        _initialOffset = this.transform.position - player.transform.position;
        _currentOffset = _initialOffset;

    }

    void MovetoPlayer()
    {
        transform.position = player.transform.position + _currentOffset;
        if (Input.GetKey(KeyCode.Q))
        {
            transform.RotateAround(player.transform.position, Vector3.up, -90 * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.E))
        {
            transform.RotateAround(player.transform.position, Vector3.up, 90 * Time.deltaTime);
        }
        _currentOffset = transform.position - player.transform.position;
        this.transform.LookAt(player.transform);
    }

    public void RotateCamera(float tempRotateVal)
    {
        Debug.Log(tempRotateVal);
        if(tempRotateVal < 0)
        {
            Debug.Log("Check");
            transform.RotateAround(player.transform.position, Vector3.up, 90 * Time.deltaTime);
        }

        if(tempRotateVal > 0)
        {
            transform.RotateAround(player.transform.position, Vector3.up, -90 * Time.deltaTime);
        }
    }

    void Update()
    {


    }

    void LateUpdate()
    {
        MovetoPlayer();
    }
}