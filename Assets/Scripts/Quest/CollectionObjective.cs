﻿using UnityEngine;
using System.Collections;

public class CollectionObjective : QuestObjective
{

    public int collectionAmount; //total amount of item needed
    public int currentAmount;  //start at 0

    public GameObject itemToCollect;

    //Create a function  that sorts through Player Item Inventory to look for item
    public override  void ItemFound(int itemID)
    {
        if (itemID == itemToCollect.GetComponent<ItemClass>()._iD)
        {
            currentAmount += 1;
        }
    }

    public override void CheckProgress()
    {
        if (currentAmount >= collectionAmount)
            _isComplete = true;
        else
            _isComplete = false;
    }

    public override void UpdateProgress()
    {
        if (_hasTimeLimit)
        {
            UpdateTime();
        }
        CheckProgress();
    }

    public override void UpdateTime()
    {
        if (_curTimeLimit >= 0.0f)
        {
            _curTimeLimit -= Time.deltaTime;
        }
        else
        {
            _curTimeLimit = 0.0f;
            //Quest Failed//
        }

    }

    public override string ToString()
    {
        return currentAmount + "/" + collectionAmount + " " + itemToCollect.name + "Found!";
    }
}
