﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PlayerQuestDatabase : MonoBehaviour {
     
    public List<Quest> playerQuestList = new List<Quest>();
    public List<Quest> completedQuestList = new List<Quest>();
    public List<Quest> activeQuestList = new List<Quest>();

    public void AddQuest(Quest AddQuest)
    {
        playerQuestList.Add(AddQuest);
    }

    public void UpdateActiveQuestList()
    {
        int tempCount = activeQuestList.Count;
        for (int i = 0; i < tempCount; i++)
        {
            activeQuestList[i].UpdateQuest();
        }
    }

    public void Found(int ID)
    {
        for (int i = 0; i < activeQuestList.Count; i++)
        {
            for (int j = 0; j < activeQuestList[i].objectives.Count; j++)
            {
                activeQuestList[i].objectives[j].ItemFound(ID);
            }
        }
    }


    void Start ()
    {
	
	}

	void Update ()
    {
	
	}
}
