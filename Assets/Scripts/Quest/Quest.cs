﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Quest
{ 
    public string _questTitle;
    public string _questDescription;

    public int _questID;
    public int _chainQuestID;
    public bool _isChainQuest;

    public bool _hasTimeLimit;
    public int _hourTimeLimit;
    public int _minuteTimeLimit;
    public int _secondTimeLimit;

    //The Current Time Limit of the Quest
    protected float _curTimeLimit;

    public List<QuestObjective> objectives;
    protected List<QuestObjective> primaryObjectives;
    protected List<QuestObjective> bonusObjectives;

    public bool ArePrimaryObjectivesComplete()
    {
        int tempCompleteCount = 0;
        for (int i = 0; i < primaryObjectives.Count; i++)
        {
            if (primaryObjectives[i].IsComplete())
            {
                tempCompleteCount += 1;
            }
        }

        if(tempCompleteCount == primaryObjectives.Count)
        {
            return true;
        }
        else return false;
    }

    public bool AreBonusObjectivesComplete()
    {
        int tempCompleteCount = 0;
        for (int i = 0; i < bonusObjectives.Count; i++)
        {
            if (bonusObjectives[i].IsComplete())
            {
                tempCompleteCount += 1;
            }
        }

        if (tempCompleteCount == bonusObjectives.Count)
        {
            return true;
        }
        else return false;
    }

    public void UpdateQuest()
    {
        _curTimeLimit -= Time.deltaTime;
        if (_curTimeLimit <= 0) _curTimeLimit = 0.0f;
        AreBonusObjectivesComplete();
        ArePrimaryObjectivesComplete();
    }


}
