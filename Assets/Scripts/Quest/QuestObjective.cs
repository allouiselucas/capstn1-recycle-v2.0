﻿using UnityEngine;
using System.Collections;

public class QuestObjective 
{
    public string _objectiveName;
    public string _objectiveDescription;

    public bool _isComplete;
    public bool _isBonusObjective;
    public bool _hasTimeLimit;

    public int _hourTimeLimit;
    public int _minuteTimeLimit;
    public int _secondTimeLimit;
    public float _curTimeLimit;

    public bool IsComplete()
    {

        return false;
    }

    public virtual void ItemFound(int ID)
    {
   

    }

    public virtual void CheckProgress()
    {

    }

    public virtual void UpdateProgress()
    {

    }

    public virtual void UpdateTime()
    {

    }

}
