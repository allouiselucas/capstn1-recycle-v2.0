﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour {

    public float buttonDelay;
    public float currDelay;
    public bool isReady;


	// Use this for initialization
	void Start () {
        buttonDelay = 8.0f;
        currDelay = buttonDelay;
        isReady = false;
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (currDelay >= buttonDelay)
        {
            currDelay -= Time.deltaTime;
            if (currDelay <= buttonDelay) isReady = true;
        }
 
	}

    public void Change()
    {
        if(isReady) StartCoroutine(ChangeLevel());
        AudioSource audio = this.GetComponent<AudioSource>();
        audio.Play();
        audio.Play(22100);  
    }

    public IEnumerator ChangeLevel()
    {
        
        yield return new WaitForSeconds(0.5f);
        float fadeTime = GameObject.Find("Fade Manager").GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);

        SceneManager.LoadScene(1);
    }

}
