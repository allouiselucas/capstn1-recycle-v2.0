﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour {

    public GameObject[] objectsToSpawn;
    public GameObject spawnObjectDestination;
    public FallSortingGameManager GameManager;

    int spawnIndex;

    public void SetSpawn()
    {
        //Random Between SpawnObjects
        int spawnObjectIndex = Random.Range(0, objectsToSpawn.Length);
        spawnIndex = spawnObjectIndex;
        //Debug.Log(objectsToSpawn.Length);
    }

    public void Spawning()
    {
        //Random.RandomRange(this.GetComponent<>)
        GameObject tempGameObject = Instantiate(objectsToSpawn[spawnIndex], this.transform.position, Quaternion.identity, this.transform.parent);
        GameManager.spawnedItems.Add(tempGameObject);
        //Assuming the tempGameObject has the MiniGameTrashBehavior Script
        tempGameObject.GetComponent<MiniGameTrashBehavior>().targetObject = spawnObjectDestination;
        tempGameObject.GetComponent<MiniGameTrashBehavior>().gameManager = GameManager;

        //Debug.Log("Check");

        //InvokeRepeating("SetSpawn", 5.0f, 1.0f);
    }

    // Use this for initialization

}
