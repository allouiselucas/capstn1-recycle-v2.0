﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneSpawner : MonoBehaviour {


    public List<ItemClass> zoneItems = new List<ItemClass>();
    public List<int> itemIDs = new List<int>();
    public List<int> maxNumberInZone;

    public ItemDatabase itemDatabase;

    //Contains all the trash bins or item minigames in a specific zone.s
    public GameObject SpawnContainer;



	// Use this for initialization
	void Start ()
    {
        itemDatabase = GameObject.FindGameObjectWithTag("ItemDatabase").GetComponent<ItemDatabase>();

        int tempCount = SpawnContainer.transform.childCount;

        for (int i = 0; i < tempCount; i++)
        {
            int tempDatabaseIndex = Random.Range(0, itemIDs.Count - 1);
            SpawnContainer.transform.GetChild(i).GetChild(0).GetComponent<MiniGameInfo>().SetItem(itemDatabase.dataBase[tempDatabaseIndex]);
        }	
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
