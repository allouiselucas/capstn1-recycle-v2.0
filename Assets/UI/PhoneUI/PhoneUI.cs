﻿    using UnityEngine;
using System.Collections.Generic;

public class PhoneUI : MonoBehaviour {

    public List<GameObject> callListUI;
    public List<GameObject> mapListUI;
    public List<GameObject> buttons;

    public List<GameObject> keyItemList;
    

    public GameObject phonePanel;
    //public GameObject questPanel;

    //public GameObject questBox;
    //public GameObject questBox2;

    public GameObject buttonUI;
    public GameObject backButton;

   


    public bool isAwake = false;

    void Start()
    {
       

        isAwake = false;
    }

    public void StartPhone()
    {
        //foreach (GameObject go in keyItemList)
        //{
        //    if(go.GetComponent<keyItemList>()._Owned == true)
        //    go.SetActive(true);
        //}

        if (!isAwake) 
        {
            //phonePanel.gameObject.SetActive(true);

            buttonUI.SetActive(false);

            foreach (GameObject go in buttons)
            {
                go.SetActive(true);
            }

            backButton.SetActive(false);

            //questPanel.SetActive(true);

            isAwake = true;

            foreach (GameObject go in callListUI)
            {
                go.SetActive(false);
            }

            foreach (GameObject go in mapListUI)
            {
                go.SetActive(false);
            }
        }

    }

    public void BackButton()
    {
        Time.timeScale = 1.0f;

        

        phonePanel.SetActive(true);

        buttonUI.gameObject.SetActive(false);

        foreach (GameObject go in buttons)
        {
            go.SetActive(true);
        }

        backButton.SetActive(false);

        //questPanel.gameObject.SetActive(true);

        isAwake = true;

        foreach (GameObject go in callListUI)
        {
            go.gameObject.SetActive(false);
        }

        foreach (GameObject go in mapListUI)
        {
            go.gameObject.SetActive(false);
        }

        //foreach (GameObject go in keyItemList)
        //{
        //    if (go.GetComponent<keyItemList>()._Owned == true)
        //        go.SetActive(true);
        //}
    }

    public void BackButtonQuest()
    {
        
        //this.GetComponent<QuestUIWindow>().OnClickActive();
    } 

    public void CallButton()
    {
        foreach (GameObject go in keyItemList)
        {
            go.SetActive(false);
        }

        foreach (GameObject go in buttons)
        {
            go.SetActive(false);
        }

        foreach (GameObject go in callListUI)
        {
            go.gameObject.SetActive(true);
        }

        foreach (GameObject go in mapListUI)
        {
            go.gameObject.SetActive(false);
        }

        backButton.SetActive(true);

        //questPanel.gameObject.SetActive(false);

    }

    public void MapButton()
    {

        foreach (GameObject go in keyItemList)
        {
            go.SetActive(false);
        }

        foreach (GameObject go in buttons)
        {
            go.SetActive(false);

        }

        foreach (GameObject go in callListUI)
        {
            go.gameObject.SetActive(false);
        }

        foreach (GameObject go in mapListUI)
        {
            go.gameObject.SetActive(true);
        }

        backButton.SetActive(true);

        //questPanel.gameObject.SetActive(false);
    }

    public void ExitButton()
    {
        phonePanel.gameObject.SetActive(false); isAwake = false;
        buttonUI.SetActive(true);
    }
}
